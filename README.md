# 文件类型和MIME类型对比

    需要支持php>=7，其他版本自行修改

#### 使用示例

    <?php
    declare(strict_types=1);
    
    use Zlf\Mime\Mime;


    //获取已收录的所有格式的mime类型
    Mime::getMimeData()
        返回示例 array
            [
                'mp4' => 'video/mp4',
                'jpg' => 'video/jpeg',
                .
                .
                .
                .
               'png' => 'video/png',
            ]
    

    //获取指定后缀的mime
    Mime::getMime('png')
        返回示例 string
            image/png
    


    //获取指定mime类型包含的格式
    Mime::getExtention('video/x-matroska')
        返回示例 [string]
            ['mkv','mk3d','mks']
                
   