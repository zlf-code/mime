<?php
declare(strict_types=1);

namespace Zlf\Mime;

class Mime
{
    private static $mime;

    public static function getMimeData()
    {
        if (is_array(self::$mime)) {
            return self::$mime;
        }
        $data = file_get_contents(dirname(__DIR__) . '/asset/mime.json');
        self::$mime = json_decode($data, true);
        return self::$mime;
    }


    /**
     * 获取文件类型
     * @param string $extention
     * @return string
     */
    public static function getMime(string $extention): string
    {
        $extention = trim(strtolower($extention));
        $all = self::getMimeData();
        return $all[$extention] ?? '';
    }


    /**
     * 获取文件类型后缀
     * @param $mime
     * @return array
     */
    public static function getExtention(string $mime): array
    {
        $mime = trim(strtolower($mime));
        $extentions = [];
        foreach (self::getMimeData() as $extention => $mimename) {
            if ($mimename === $mime) {
                $extentions[] = $extention;
            }
        }
        return $extentions;
    }
}